
class Calculator:
    def __init__(self, x):
        self.x = x

    def __add__(self, other):
        return self.x + other

    def __sub__(self, other):
        return self.x - other

    def __mul__(self, other):
        return self.x * other

    def __truediv__(self, other):
        return self.x / other


def main():
    operation = input('Input number / operation(+-/*) / number: ')
    try:
        x, oper, y = operation.split(' ')
        x = Calculator(int(x))
        y = int(y)

    except Exception:
        print('\n', ' Something is wrong, try again')
        return main()

    if oper == '+': print(x.__add__(y))
    elif oper == '-': print(x.__sub__(y))
    elif oper == '/': print(x.__truediv__(y))
    elif oper == '*': print(x.__mul__(y))
    else:
        print('\n', ' Something is wrong, try again')
        return main()


if __name__ == '__main__':
    main()


