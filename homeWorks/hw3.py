

class Bank:
    def __init__(self, name, balance=0.0):
        self._name = name
        self._balance = balance

    def __str__(self):
        return f'{self._name}, balance: {self._balance}'

    def add_money(self):
        cash_money = int(input('How much money you want to add: '))
        self._balance += cash_money
        return self._balance

    def _kill(self):
        self._balance = 0.0
        return self._balance

    def __jackpot(self):
        self._balance *= 10
        return self._balance

    def _family_balance(self, wife):
        self._balance += wife._balance
        return self._balance

dmitriy = Bank('Dmitriy', 500)
wife = Bank('Olga', 100)

print(dmitriy)
print(dmitriy._family_balance(wife))

print(dmitriy._Bank__jackpot())

print(dmitriy._kill())

dmitriy.add_money()
print(dmitriy._balance)
